%%%-------------------------------------------------
%%% Autor: Staffan Arvidsson 
%%% Serial implementation of traversal of the de Bruijn graph  
%%%
%%%-------------------------------------------------

-module(trav_deBruijn).
-compile(export_all).


%%% Each record in the ETS table: {key, unique?(T/F), PrevChar, NextChar, Mult}




%%-----------------------------------------------
%% Creates all contigs from the given ETS table 
%%-----------------------------------------------

createAllUUContigs(TableName,K) ->
	{uu_contigs, loopTraverse(TableName,[], K)}.

loopTraverse(Table, ContigList, K) ->
	case ets:first(Table) of
		'$end_of_table' -> 	% no more entries!
			ContigList;
		Key ->
			[{Start_seq,_,Prev,Next,_}] = ets:lookup(Table, Key),
			ets:delete(Table, Key),
			
			{left_res, Left_extend} = travLeft([Prev|Start_seq], Table, K),
			{right_res, Right_extend} = travRight(Start_seq ++ string:chars(Next,1), Table, 1),
			loopTraverse(Table, [merge(Left_extend,Right_extend,K)|ContigList],K)
			
	end.

%%-----------------------------------------------
%% traverse left
%%-----------------------------------------------
travLeft(Contig, Table, K) ->
	First_K = lists:sublist(Contig, K),
	Match = ets:lookup(Table, First_K),
	
	if Match == [] ->
		%Done - return the contig
		{left_res, Contig};
	true ->
		[{_K_mer,_,Prev,_,_}] = Match,
		ets:delete(Table, First_K),
		travLeft([Prev|Contig], Table, K)
	end.
	
%%-----------------------------------------------
%% traverse right
%%-----------------------------------------------
travRight(Contig, Table, Iteration) -> 
	{_First_part, Last_K} = lists:split(Iteration, Contig),
	
	Match = ets:lookup(Table, Last_K),
	if Match == [] ->
		%Done - return the contig
		{right_res, Contig};
	true ->
		[{_K_mer,_,_,Next,_}] = Match,
		ets:delete(Table, Last_K),
		travRight(Contig ++ string:chars(Next,1), Table, Iteration+1)
	end.

%%-----------------------------------------------
%% merge - do no types of checking, that's up to the caller!
%%-----------------------------------------------
merge(Left,Right,K) ->
	{_, Right_extend} = lists:split(K, Right),
	Left ++ Right_extend.

%%-----------------------------------------------
%% Test
%%-----------------------------------------------
test() ->
	DeBruijnGraph = deBruijn:start(["input_fasta.txt", "10"]),
	createAllUUContigs(DeBruijnGraph, 10).
			