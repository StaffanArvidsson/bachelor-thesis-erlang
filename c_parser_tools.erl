%%%-------------------------------------------------
%%% Author: Staffan Arvidsson
%%% Parser tools for the concurrent version 
%%% Spawn this as its own process 
%%%	
%%%-------------------------------------------------

-module(c_parser_tools).
-compile(export_all).

%%-------------------------------------
%% interface methods
%%-------------------------------------

return_data(Return_PID, Result) ->
	Return_PID ! {data, Result}.

signal_done(Return_PID) ->
	Return_PID ! {done, self()}.

signal_error(Return_PID, Error) ->
	Return_PID ! Error.


%%-----------------------------------------------
%% spawn this to it's own process! 
%%-----------------------------------------------


parse_chunk_wise(FilePath, Return_PID, N, Dest_PIDs) ->
	if N=<0 ->
		signal_error(Return_PID, {error, "Cannot pass a chunk_size less than 1!"});
	true ->
		case file:open(FilePath, [read]) of
			{ok, IoDev} ->
				
				parse_chunk_wise(IoDev, N, [], Dest_PIDs, length(Dest_PIDs)),
				signal_done(Return_PID),
				file:close(IoDev);
				
			{error, enoent} ->
				io:format("~n ########### The file specified cannot be found ###########~n~n"),
				signal_error(Return_PID, {error, enoent});
			{error, Reason} ->
				signal_error(Return_PID, {error, Reason})
		end
	end.


parse_chunk_wise(IoDev, N, Next_name, Dest_PIDs, NextPID) when NextPID =< 0 ->
	parse_chunk_wise(IoDev, N, Next_name, Dest_PIDs, length(Dest_PIDs));
parse_chunk_wise(IoDev, N, Next_name, Dest_PIDs, NextPID) ->
	case scanN_fasta(N, IoDev, [], Next_name, []) of
		{eof, [{[],[]}]} ->
			ok;
			%signal_done(Return_PID);
		{eof, Result} ->
			return_data(lists:nth(NextPID, Dest_PIDs), Result),
			ok;
			%signal_done(Return_PID);
		{_, New_next_name, Result} ->
			return_data(lists:nth(NextPID, Dest_PIDs), Result),
			parse_chunk_wise(IoDev, N, New_next_name, Dest_PIDs, NextPID-1);
		{error, Reason} ->
			signal_error(hd(Dest_PIDs), {error, Reason})
	end.
		
	
%% will return {IoDev, [{Name, Sequence}]} IF not encountered eof 
%% IF eof reached: returns {eof, [{Name, Sequence}]}
scanN_fasta(N,IoDev, Result, Next_name,[]) when N =< 0 ->
	{IoDev, Next_name, Result};
scanN_fasta(N, IoDev, Result, Temp_name, Temp_seq) ->
	case file:read_line(IoDev) of
		{ok, Data} ->
			[First_char|_] = Data,
			if First_char == $> -> %% Name!
				%------------------------------------
				%Check if there is an old record to add
				
				if Temp_name == [] -> % No old record
						scanN_fasta(N, IoDev, Result, string:strip(tl(Data), right,$\n), Temp_seq);
					
					true -> % old record that should be put in the result!
						scanN_fasta(N-1,IoDev, [{Temp_name, Temp_seq}|Result], string:strip(tl(Data), right,$\n), [])
				end;
				%-------------------------------------
			First_char == $\n -> % just continue to next row!
				scanN_fasta(N, IoDev,Result,Temp_name,Temp_seq);
			
			true -> % Data!!
				scanN_fasta(N, IoDev, Result, Temp_name, lists:append(Temp_seq, string:strip(Data,right,$\n)))
			end;
	
		eof -> % Add last record to result and return it
			{eof,[{Temp_name, Temp_seq}| Result]};	
		{error, Reason} ->
			{error, Reason}
	end.