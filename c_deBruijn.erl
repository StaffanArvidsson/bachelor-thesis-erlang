%%%-------------------------------------------------
%%%	Concurrent version!!
%%% Uses an ETS table to store the k-mere table
%%% ETS provides constant lookup speed and support for concurrency
%%% uses the heuristic approach (unique k-meres and prev/next char)
%%%	
%%% Each record in the ETS table: {key, unique?(T/F), PrevChar, NextChar, Mult}
%%%	
%%%-------------------------------------------------

-module(c_deBruijn).
-compile(export_all).


%%-----------------------------------------------
%% Macros definitions
%%-----------------------------------------------

-define(N_PROC, 5).
-define(TABLE, k_table).
-define(BUCKET_SIZE, 100).
-define(OUT_FILE, "./output/outfile_concurrent.txt").


% erlc parse_tools.erl utils_deBruijn.erl con_deBruijn.erl  % does not work!
% timer:tc(con_deBruijn, start, [["./input_files/input_small.txt", "8"]]).
% time erl +A 20 -run c_deBruijn start ./input/ERR_50k_reads.fasta 25 -s init stop -noshell


%%-----------------------------------------------
%% Start up function 
%%-----------------------------------------------
start( [FilePath|K_string] ) ->
	{K, _} = string:to_integer(hd(K_string)),
	
	genDeBruijn(FilePath,K).


%%-----------------------------------------------
%% Starting up function (Main function/process)
%%-----------------------------------------------

genDeBruijn(FilePath, K) -> 
	% Delete the old table if present
	delete_previous_stuff(),
	
	% create a new table
	ets:new(?TABLE, [set, public, named_table]),
	
	% Spawn worker processes
	Worker_PIDs = spawn_and_link_workers(?N_PROC, K),
	
	distribute_work(FilePath, Worker_PIDs, ?BUCKET_SIZE),

	% Signal 'stop' to all workers
	signal_stop_to_Workers(Worker_PIDs),
	
	% Wait for all workers to finish
	wait_for_workers_to_finish(Worker_PIDs),

	clean_tree(),
	
	{uu_contigs, UUContig_list} = trav_deBruijn:createAllUUContigs(?TABLE,K),

	utils_deBruijn:printContigs(?OUT_FILE, UUContig_list).
	


% --------------------------------------

wait_for_workers_to_finish([]) ->
	ok;
wait_for_workers_to_finish(Worker_PIDs) ->
	receive
		{stop, PID}->
			wait_for_workers_to_finish(lists:delete(PID, Worker_PIDs));
		{'EXIT', PID ,normal} -> % no problem!
			wait_for_workers_to_finish(lists:delete(PID, Worker_PIDs));
		Unknown ->
			io:format("Unknown message to MAIN??? ~p~n", [Unknown])
	after 6000000 ->
		io:format("No result in Main....")
	end.


%%-----------------------------------------------

% Spawn worker processes that will do the work
spawn_and_link_workers(Num_proc, K) ->
	if Num_proc =< 0 ->
		{error, "Cannot produce 0 or less workers"};
	true ->
		spawn_and_link_workers(Num_proc, K, [])
	end.

spawn_and_link_workers(0, _K, WorkerList) -> WorkerList;
spawn_and_link_workers(Num_proc, K, WorkerList) ->
	Work_PID = spawn_link(?MODULE, worker_proc, [self(), K]),
	spawn_and_link_workers(Num_proc-1, K, [Work_PID|WorkerList]).


%%-----------------------------------------------

distribute_work(FilePath, Worker_PIDs, Chunk_size) ->
	% spawn parser process!
	Parser_PID = spawn_link(c_parser_tools, parse_chunk_wise, [FilePath, self(), Chunk_size, Worker_PIDs]),
	% go to receive function, will distribute the work
	wait_for_parser(Parser_PID, Worker_PIDs).



wait_for_parser(Parser_PID, Worker_PIDs) ->
	receive
		{done, Parser_PID} ->
			ok;
		{error, enoent} ->
			signal_stop_to_Workers(Worker_PIDs),
			exit(normal);
		Unknown ->
			io:format("Wait for parser received unknown msg; ~p", [Unknown])
	end.


%%-----------------------------------------------

signal_stop_to_Workers(Worker_PIDs) ->
	lists:foldl((fun(PID, _) -> stopWorkerProc(PID), 1 end), 1, Worker_PIDs).
	

%%-----------------------------------------------

%% Remove old tables and the outfile so that the execution can run smoothly. 
delete_previous_stuff() ->
	Table_exist = lists:any((fun(X) -> X == ?TABLE end), ets:all()),
	if Table_exist ->
		ets:delete(?TABLE);
	true ->
		ok
	end,
	File_exists = filelib:is_file(?OUT_FILE),
	if File_exists ->
		file:delete(?OUT_FILE),
		ok;
	true ->
		ok
	end.

%%-----------------------------------------------

% Removes all non-unique k-meres from the table - forms the final k-mer table!
clean_tree() ->
	ets:match_delete(?TABLE, {'_', false,'_','_','_'}).




%%-----------------------------------------------
%% Worker process  
%%-----------------------------------------------

worker_proc(Main_PID, K) ->
	receive
		stop ->
			sendEndW(Main_PID),
			exit(normal);
		{data, Data} ->

			add_to_K_table(Data,K),
			
			worker_proc(Main_PID, K);
		_ ->
			exit("Unknown message received in Worker")
	end.


%%-----------------------------------------------
%% Produce k-mers

%% Loop over the list of fasta sequences and do work on each sequence
add_to_K_table([],_K) ->
	ok;
add_to_K_table([{_, FastaSeq}|Tail_list],K) ->
	addFastaSeqToTable(FastaSeq,K),
	add_to_K_table(Tail_list, K).



%% Will take one Fasta String and process it and update the ETS table 
addFastaSeqToTable(FastaSeq, K) ->
	generateHashIndexes(tl(FastaSeq), K, hd(FastaSeq)).


%% Recursively goes through an individual dna-seqence and adds to the ETS table
generateHashIndexes(Seq, K, PrevChar) ->
	CheckRun = length(Seq) < K+2,
	if CheckRun ->
			ok;		% done
		true ->
			% still some part of the string left	
			{FirstK,[NextChar|_]} = lists:split(K, Seq),
			add_to_table( FirstK, PrevChar, NextChar ),					% Add to the GB tree
			generateHashIndexes(tl(Seq),K, hd(FirstK))
	end.


add_to_table( K_mer, PrevChar, NextChar ) ->
	% check if it's already present
	K_exists = ets:lookup(?TABLE, K_mer),
	
	if []== K_exists ->
		% no such k-mere exists yet!
		ets:insert(?TABLE, {K_mer, true, PrevChar, NextChar, 1});
	true ->
		% have to check what's in that record!
		{_Seq, Unique, Prev, Next, _Mult} = hd(K_exists),
		
		if Unique == false ; Next /= NextChar ; Prev /= PrevChar ->
			% this means that it's a non unique UU contig
			ets:update_element(?TABLE, K_mer, {2,false});
		true ->
			% update the multiplicity of that k-mere!
			ets:update_counter(?TABLE, K_mer, {5,1})				% Update position 5 (multiplicity) with 1
		end
	end.




%%--------------------------------------------------------------------------
%% Interface methods taking care of the protocol for sending messages between processes 
%%--------------------------------------------------------------------------
% Worker process sends result to main process
sendResultW(Main_PID, Res) ->
	Main_PID ! {result, Res}.

% Worker proc sends 'end' to main proc
sendEndW(Main_PID) ->
	Main_PID ! {stop, self()}.

sendDataToProcess(Worker_PID, Data) ->
	Worker_PID ! {data, Data}.

stopWorkerProc(Worker_PID) ->
	Worker_PID ! stop.




