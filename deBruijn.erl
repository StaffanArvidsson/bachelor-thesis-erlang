%%%-------------------------------------------------
%%% Serial implementation of de Bruijn graphs 
%%%	Uses an ETS table to store the k-mer table
%%% ETS provides constant lookup speed and support for concurrency
%%% uses the heuristic approach (unique k-mers and prev/next char)
%%%	
%%% Each record in the ETS table: {key, unique?(T/F), PrevChar, NextChar, Mult}
%%%-------------------------------------------------

% compile:		erlc deBruijn.erl
% time erl +A 20 -run deBruijn start ./input/ERR_50k_reads.fasta 25 -s init stop -noshell


-module(deBruijn).
-compile(export_all).


%%-----------------------------------------------
%% Macros definitions
%%-----------------------------------------------

-define(OUT_FILE, "./output/output_UUContigs_serial.txt").
-define(TABLE, k_mere_table).


%%-----------------------------------------------
%% Main function that computes everything 
%%-----------------------------------------------

start( [FilePath|K_string] ) ->
	delete_previous_stuff(),
	ets:new(?TABLE, [set, private, named_table]),				% change to "public" for concurrent version
	
	case parser_tools:parseFasta(FilePath) of					% Get the fasta list
		{fastaSeq, FastaList} ->
			ok;
		{error, Reason} ->
			FastaList = 0,
			io:format("Error: ~p~n", [Reason]),
			error(Reason)
	end,
	
	{K, _} = string:to_integer(hd(K_string)),
	createDeBruijn(FastaList, K),								% Create the hash indexes in the k-mer table!

	clean_tree(),												% One pass over the table and remove all seq without unique prev/next
	
	{uu_contigs, UUContig_list} = trav_deBruijn:createAllUUContigs(?TABLE,K),

	utils_deBruijn:printContigs(?OUT_FILE, UUContig_list).

% --------------------------------------

%% Remove old tables and old outfiles if present
delete_previous_stuff() ->
	Table_exist = lists:any((fun(X) -> X == ?TABLE end), ets:all()),
	if Table_exist ->
		ets:delete(?TABLE);
	true ->
		ok
	end,
	File_exists = filelib:is_file(?OUT_FILE),
	if File_exists ->
		file:delete(?OUT_FILE),
		ok;
	true ->
		ok
	end.

% --------------------------------------

%% loops over the list of fasta records and updates the table 
createDeBruijn([], _K) ->
	ok;
createDeBruijn ([{_, FastaSeq}|FastaList], K) ->
	addFastaSeqToTable(FastaSeq,K),
	createDeBruijn(FastaList, K).
	

%% Will take one Fasta String and process it and update the ETS table 
addFastaSeqToTable(FastaSeq, K) ->
	generateHashIndexes(tl(FastaSeq), K, hd(FastaSeq)).


%% Recursively goes through an individual dna-seqence and adds to the ETS table
generateHashIndexes(Seq, K, PrevChar) ->
	CheckRun = length(Seq) < K+2,
	if CheckRun ->
			ok;		% done
		true ->
			% still some part of the string left	
			{FirstK,[NextChar|_]} = lists:split(K, Seq),
			add_to_table( FirstK, PrevChar, NextChar ),					% Add to the GB tree
			generateHashIndexes(tl(Seq),K, hd(FirstK))
	end.


add_to_table( K_mer, PrevChar, NextChar ) ->
	% check if it's already present
	K_exists = ets:lookup(?TABLE, K_mer),
	
	if []== K_exists ->
		% no such k-mere exists yet!
		ets:insert(?TABLE, {K_mer, true, PrevChar, NextChar, 1});
	true ->
		% have to check what's in that record!
		{_Seq, Unique, Prev, Next, _Mult} = hd(K_exists),
		
		if Unique == false ; Next /= NextChar ; Prev /= PrevChar ->
			% this means that it's a non unique UU contig
			ets:update_element(?TABLE, K_mer, {2,false});
		true ->
			% update the multiplicity of that k-mere!
			ets:update_counter(?TABLE, K_mer, {5,1})	% Update position 2 (multiplicity) with 1
		end
	end.

% --------------------------------------	
% Removes all records that do not have unique k-mers (where prev/next bases has proven to not match)
clean_tree() ->
	ets:match_delete(?TABLE, {'_', false,'_','_','_'}).	
