%%%-------------------------------------------------
%%% Autor: Staffan Arvidsson 
%%% Serial implementation of a parser for fasta files  
%%%
%%%-------------------------------------------------

-module(parser_tools).
-compile(export_all).


%%-----------------------------------------------
%% Interface 
%%-----------------------------------------------


parseFasta(File) ->
	case file:open(File, [read]) of
		{ok, IoDev} ->
			{fasta, Seq_list} = parseFastaToPair(IoDev),
			file:close(IoDev),
			{fastaSeq, Seq_list};
		{error, enoent} ->
			io:format("~n ########### The file specified cannot be found ###########~n~n"),
			exit(error);
		{error, Reason} ->
			{error, Reason}
	end.

%%-----------------------------------------------
%% Parser part!
%%-----------------------------------------------


parseFastaToPair(IoDev)->
	{fasta, scan_fasta(IoDev, [], [],[])}.
	
%% Give you a list of {Name, Sequence}
scan_fasta(IoDev, Result, Temp_name, Temp_seq) ->
	case file:read_line(IoDev) of
		{ok, Data} ->
			[First_char |_] = Data,
			if First_char == $> -> %% Name!
				
				%Check if there is an old record to add
				if Temp_name == [] -> % No old record
						scan_fasta(IoDev, Result, string:strip(tl(Data), right,$\n), Temp_seq);
					true -> % old record that should be put in the result!
						scan_fasta(IoDev, [{Temp_name, Temp_seq}|Result], string:strip(tl(Data), right,$\n), [])
				end;
			First_char == $\n -> % just continue to next row!
				scan_fasta(IoDev,Result,Temp_name,Temp_seq);
			
			true -> % Data!!
				scan_fasta(IoDev, Result, Temp_name, lists:append(Temp_seq, string:strip(Data,right,$\n)))
			end;
	
		eof -> % Add last record to result and return it
			[{Temp_name, Temp_seq}| Result];
		{error, Reason} ->
			{error, Reason}
	end.
