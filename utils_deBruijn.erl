%%%-------------------------------------------------
%%% Autor: Staffan Arvidsson 
%%% Serial implementation of printing contigs to file  
%%%
%%%-------------------------------------------------

-module(utils_deBruijn).
-compile(export_all).

%%-----------------------------------------------
%% Print Contigs 
%%-----------------------------------------------

printContigs(FileName, ListOfContigs) ->
	File_exists = filelib:is_file(FileName),
	if File_exists ->
		file:delete(FileName),
		writeContigs(FileName,ListOfContigs,1);
	true ->
		writeContigs(FileName, ListOfContigs,1)
	end.
	
writeContigs(_FileName, [], _Num) -> ok;
writeContigs(FileName, [H|T], Num) ->
	file:write_file(FileName, [<<">Contig ">>, integer_to_list(Num), <<"\n">>], [append, binary, delayed_write]),
	file:write_file(FileName, [list_to_binary(H),<<"\n">>], [append, binary, delayed_write]),
	writeContigs(FileName, T, Num+1).			